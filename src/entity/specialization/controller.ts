import SpecializationModel from "./model";
import specializationSchema from "./schema";

class SpecializationController {
  public static async list(req, res): Promise<void> {
    const { skip, limit } = req.query;
    const specializations = await SpecializationModel.find()
      .sort({
        createdAt: -1,
      })
      .skip(parseInt(skip))
      .limit(parseInt(limit));
    res.status(200).send({ status: "success", data: specializations || [] });
  }

  public static async create(req, res): Promise<void> {
    const { body } = req;

    try {
      const validatedSpecialization = await specializationSchema.validate(body);
      const specialization = new SpecializationModel(validatedSpecialization);
      await specialization.save();
      console.log(`Specialization created successfully`);
      res.status(200).send({ status: "success" });
    } catch (err) {
      console.log(`Error creating specialization :${err}`);
      res.status(500).send({ status: "failure", error: err });
    }
  }
}

export default SpecializationController;
