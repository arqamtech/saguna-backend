import specializationController from "./controller";
const routes = require("express").Router();

routes.get("/specializations", specializationController.list);
routes.post("/specializations", specializationController.create);

export default routes;
