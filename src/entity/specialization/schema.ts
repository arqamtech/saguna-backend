import * as yup from 'yup';

const specializationSchema = yup
  .object()
  .shape({
    specializationName: yup.string().required().max(40).default("").trim(),
  })
  .noUnknown();

export default specializationSchema;
