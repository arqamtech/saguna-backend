import mongoose from "mongoose";

const SpecializationSchema = new mongoose.Schema(
  {
    specializationName: {
      type: String,
      required: true,
      unique: true,
      index: true,
    },
  },
  {
    timestamps: true,
  }
);

export default mongoose.model("specialization", SpecializationSchema);
