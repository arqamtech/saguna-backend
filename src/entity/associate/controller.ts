import AssociateModel from "./model";
import associateSchema from "./schema";

class AssociateController {
  public static async list(req, res): Promise<void> {
    const { skip, limit } = req.query;
    const associates = await AssociateModel.find()
      .sort({
        createdAt: -1,
      })
      .skip(parseInt(skip))
      .limit(parseInt(limit))
      .populate("specializations");
    res.status(200).send({ status: "success", data: associates || [] });
  }

  public static async create(req, res): Promise<void> {
    const { body } = req;
    try {
      const validatedAssociate = await associateSchema.validate(body);
      const associate = new AssociateModel(validatedAssociate);
      await associate.save();
      res.status(200).send({ status: "success" });
    } catch (err) {
      console.log(`Error creating associate :${err}`);
      res.status(500).send({ status: "failure", error: err });
    }
  }

  public static async getById(req, res): Promise<void> {
    const { associateId } = req.params;
    try {
      const associate = await AssociateModel.findById(associateId).populate(
        "specializations"
      );
      res.status(200).send({ status: "success", data: associate });
    } catch (err) {
      console.log(`Error getting associate :${err}`);
      res.status(500).send({ status: "failure", error: err });
    }
  }

  public static async deleteById(req, res): Promise<void> {
    const { associateId } = req.params;
    try {
      await AssociateModel.findByIdAndDelete(associateId);
      res.status(200).send({ status: "success" });
    } catch (err) {
      console.log(`Error getting associate :${err}`);
      res.status(500).send({ status: "failure", error: err });
    }
  }

  public static async search(req, res): Promise<void> {
    const { query } = req.params;
    try {
      const associates = await AssociateModel.find({
        associateName: { $regex: query, $options: "i" },
      });
      res.status(200).send({ status: "success", data: associates });
    } catch (err) {
      console.log(`Error getting associate :${err}`);
      res.status(500).send({ status: "failure", error: err });
    }
  }

  public static async update(req, res): Promise<void> {
    const { body } = req;
    try {
      const { id } = body;
      const associates = await AssociateModel.findByIdAndUpdate(id, body);
      res.status(200).send({ status: "success", data: associates });
    } catch (err) {
      console.log(`Error updating associate :${err}`);
      res.status(500).send({ status: "failure", error: err });
    }
  }
}

export default AssociateController;
