import associateController from "./controller";
const routes = require("express").Router();

routes.post("/associates", associateController.create);
routes.get("/associates", associateController.list);
routes.get("/associate/:associateId", associateController.getById);
routes.delete("/associate/:associateId", associateController.deleteById);
routes.get("/search/:query", associateController.search);
routes.put("/associates", associateController.update);

export default routes;
