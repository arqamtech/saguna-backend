import * as yup from "yup";

const associateSchema = yup
  .object()
  .shape({
    associateName: yup.string().required().max(40).default("").trim(),
    phone: yup.number().required(),
    address: yup.string().required().default("").trim(),
    specializations : yup.array()
  })
  .noUnknown();

export default associateSchema;
