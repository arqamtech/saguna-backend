import mongoose from "mongoose";

const AssociateSchema = new mongoose.Schema(
  {
    associateName: {
      type: String,
      required: true,
      unique: true,
      index: true,
    },
    phone: {
      type: Number,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    specializations: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'specialization'
      }
    ]

  },
  {
    timestamps: true,
  }
);

export default mongoose.model("associate", AssociateSchema);
