import { Router } from "express";
import speclizationRoutes from "./entity/specialization/routes";
import associateRoutes from "./entity/associate/routes";

const routes = Router();

routes.get("/", (req, res) => {
  res.status(200).json({ message: "Connected!" });
});
routes.use(speclizationRoutes);
routes.use(associateRoutes);
export default routes;
