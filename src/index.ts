import * as dotenv from "dotenv";
import express from "express";
import cors from "cors";
import helmet from "helmet";
import { Router } from "express";
import routes from "./routes";
import mongoose from "mongoose";
import { urlencoded, json } from "body-parser";
dotenv.config();

if (!process.env.PORT) {
  process.exit(1);
}

const PORT: number = parseInt(process.env.PORT as string, 10);
const MONGO_CONNECTION: string = process.env.MONGO_CONNECTION;

const router = Router();
const app = express();

router.use(routes);
mongoose.connect(
  MONGO_CONNECTION,
  {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (err) =>
    err
      ? console.log("Error Connecting to database", err)
      : console.log("Connection to database successful")
);
app.use(helmet());
app.use(cors());
app.use(
  urlencoded({
    extended: true,
  })
);
app.use(json());

app.use("/", router);

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
